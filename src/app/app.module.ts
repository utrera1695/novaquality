import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { DragulaModule } from "ng2-dragula";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./login/login.component";
import { HeaderComponent } from "./header/header.component";
import { ContentComponent } from "./content/content.component";

import { SegmentationComponent } from "./segmentation/segmentation.component";
import { EditSegmentComponent } from "./segmentation/edit-segment/edit-segment.component";
import { NewsegmentationComponent } from "./segmentation/newsegmentation/newsegmentation.component";
import { DatastructureComponent } from "./segmentation/datastructure/datastructure.component";

import { ConditionsComponent } from "./conditions/conditions.component";
import { EditConditionComponent } from "./conditions/edit-condition/edit-condition.component";
import { NewConditionComponent } from "./conditions/new-condition/new-condition.component";

import { ClausesComponent } from "./clauses/clauses.component";
import { RunsegmentationComponent } from "./segmentation/runsegmentation/runsegmentation.component";
import { NgZorroAntdModule, NZ_I18N, en_US } from "ng-zorro-antd";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { registerLocaleData } from "@angular/common";
import en from "@angular/common/locales/en";
import { CalendarComponent } from "./segmentation/runsegmentation/calendar/calendar.component";
import { NewclausesComponent } from "./clauses/newclauses/newclauses.component";
import { GoogleChartsModule } from "angular-google-charts";
import { TreemapComponent } from "./clauses/treemap/treemap.component";
import { ClausesCamposComponent } from "./clauses/clauses-campos/clauses-campos.component";
import { DragDropModule } from "@angular/cdk/drag-drop";

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    ContentComponent,
    SegmentationComponent,
    ConditionsComponent,
    ClausesComponent,
    NewsegmentationComponent,
    DatastructureComponent,
    EditSegmentComponent,
    EditConditionComponent,
    NewConditionComponent,
    RunsegmentationComponent,
    CalendarComponent,
    NewclausesComponent,
    TreemapComponent,
    ClausesCamposComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    NgZorroAntdModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    GoogleChartsModule.forRoot(),
    DragulaModule.forRoot(),
    DragDropModule
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }],
  bootstrap: [AppComponent]
})
export class AppModule {}
