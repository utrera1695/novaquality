import { Component, OnInit } from "@angular/core";
import { CdkDragDrop } from "@angular/cdk/drag-drop";

@Component({
  selector: "app-clauses",
  templateUrl: "./clauses.component.html",
  styleUrls: ["./clauses.component.scss"]
})
export class ClausesComponent implements OnInit {
  inputValue: any = {
    condition: "(( ´col 1´ >0) and (´col 2´ <0) or (left (´col 3´,3)=´ORG´)",
    parameter: "(´col 4´/5)"
  };
  cond = [];
  colums = [
    {
      name: "Col 1",
      type: "Number"
    },
    {
      name: "Col 2",
      type: "Number"
    },
    {
      name: "Col 3",
      type: "Number"
    },
    {
      name: "Col 4",
      type: "Number"
    },
    {
      name: "Col 5",
      type: "Number"
    }
  ];
  option: any = 0;
  constructor() {}

  ngOnInit(): void {}

  changeOption(value) {
    if(this.option == 1){
      this.option = 0;
    }else{
      this.option = value;
    }
  }
  drop(event: CdkDragDrop<string[]>) {
    console.log(event);
    if (event.previousContainer !== event.container) {
      this.inputValue.condition +=
        "´" +
        event.item.element.nativeElement.getElementsByTagName("td")[0]
          .innerText +
        "´";
    }
  }
}
