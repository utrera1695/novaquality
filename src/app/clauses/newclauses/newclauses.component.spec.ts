import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewclausesComponent } from './newclauses.component';

describe('NewclausesComponent', () => {
  let component: NewclausesComponent;
  let fixture: ComponentFixture<NewclausesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewclausesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewclausesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
