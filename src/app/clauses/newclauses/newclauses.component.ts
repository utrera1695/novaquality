import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-newclauses",
  templateUrl: "./newclauses.component.html",
  styleUrls: ["./newclauses.component.scss"]
})
export class NewclausesComponent implements OnInit {
  isVisible = false;
  selectedValue:any = 'AND';
  constructor() {}

  ngOnInit(): void {}
  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    console.log("Button ok clicked!");
    this.isVisible = false;
  }

  handleCancel(): void {
    console.log("Button cancel clicked!");
    this.isVisible = false;
  }
}
