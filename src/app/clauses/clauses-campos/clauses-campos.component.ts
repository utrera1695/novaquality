import { Component, OnInit } from "@angular/core";

import { DragulaService } from "ng2-dragula";
@Component({
  selector: "app-clauses-campos",
  templateUrl: "./clauses-campos.component.html",
  styleUrls: ["./clauses-campos.component.scss"]
})
export class ClausesCamposComponent implements OnInit {
  constructor(private dragulaService: DragulaService) {}

  ngOnInit(): void {
    this.dragulaService.createGroup("COLUMS", {
      removeOnSpill: false
    });
  }
}
