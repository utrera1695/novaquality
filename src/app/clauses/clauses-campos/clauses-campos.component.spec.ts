import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClausesCamposComponent } from './clauses-campos.component';

describe('ClausesCamposComponent', () => {
  let component: ClausesCamposComponent;
  let fixture: ComponentFixture<ClausesCamposComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClausesCamposComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClausesCamposComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
