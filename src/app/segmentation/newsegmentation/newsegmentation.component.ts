import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-newsegmentation",
  templateUrl: "./newsegmentation.component.html",
  styleUrls: ["./newsegmentation.component.scss"]
})
export class NewsegmentationComponent implements OnInit {
  isVisible = false;
  constructor() {}

  ngOnInit(): void {}
  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    console.log("Button ok clicked!");
    this.isVisible = false;
  }

  handleCancel(): void {
    console.log("Button cancel clicked!");
    this.isVisible = false;
  }
}
