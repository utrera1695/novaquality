import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsegmentationComponent } from './newsegmentation.component';

describe('NewsegmentationComponent', () => {
  let component: NewsegmentationComponent;
  let fixture: ComponentFixture<NewsegmentationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsegmentationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsegmentationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
