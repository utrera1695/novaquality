import { Component, OnInit } from "@angular/core";

import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
@Component({
  selector: "app-datastructure",
  templateUrl: "./datastructure.component.html",
  styleUrls: ["./datastructure.component.scss"]
})
export class DatastructureComponent implements OnInit {
  isVisible = false;
  constructor() {}

  ngOnInit(): void {}
  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    console.log("Button ok clicked!");
    this.isVisible = false;
  }

  handleCancel(): void {
    console.log("Button cancel clicked!");
    this.isVisible = false;
  }
}
