import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { Random } from "mockjs";
@Component({
  selector: "app-segmentation",
  templateUrl: "./segmentation.component.html",
  styleUrls: ["./segmentation.component.scss"]
})
export class SegmentationComponent implements OnInit {
  @Input() segmentModel: number;
  @Output() segmentChange = new EventEmitter();
  isVisible: boolean = false;
  data: any = [];
  segments: any = [];
  constructor() {
    for (let i = 0; i < 6; i++) {
      let segment = {
        namesegmentation: Random.word(),
        namedescription: Random.word(),
        contentsegmentation: Random.word(),
        unit: Random.natural(1, 7),
        time: Random.natural(1, 3),
        from: Random.natural(1, 3),
        customControlValidation1: true,
        validationServer04: "",
        restcondition: true,
        filter: false
      };
      this.segments.push(segment);
    }
  }

  ngOnInit(): void {}
  modal(data?: any) {
    this.isVisible = !this.isVisible;
    if (data) this.data = data;
  }
  changeStyle(i) {
    if (this.segmentModel == i) {
      return { "background-color": "#0062cc", color: "white" };
    }
  }
  valueChanged(i, selected) {
    if(i){
      this.segmentChange.emit(i);
    }
    this.segmentModel = selected;
  }
}
