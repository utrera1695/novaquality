import { Component, OnInit } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-runsegmentation",
  templateUrl: "./runsegmentation.component.html",
  styleUrls: ["./runsegmentation.component.scss"]
})
export class RunsegmentationComponent implements OnInit {
  isVisible = false;
  constructor(private modalService: NgbModal) {}

  ngOnInit(): void {}
  open(content) {
    this.modalService.open(content, {
      size: "xl"
    });
  }
  onValueChange(value: Date): void {
    console.log(`Current value: ${value}`);
  }

  onPanelChange(change: { date: Date; mode: string }): void {
    console.log(`Current value: ${change.date}`);
    console.log(`Current mode: ${change.mode}`);
  }
  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    console.log("Button ok clicked!");
    this.isVisible = false;
  }

  handleCancel(): void {
    console.log("Button cancel clicked!");
    this.isVisible = false;
  }
}
