import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RunsegmentationComponent } from './runsegmentation.component';

describe('RunsegmentationComponent', () => {
  let component: RunsegmentationComponent;
  let fixture: ComponentFixture<RunsegmentationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RunsegmentationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RunsegmentationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
