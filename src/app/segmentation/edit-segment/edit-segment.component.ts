import { Component, OnInit, Input } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-edit-segment",
  templateUrl: "./edit-segment.component.html",
  styleUrls: ["./edit-segment.component.scss"]
})
export class EditSegmentComponent implements OnInit {
  @Input() title: string;
  isVisible = false;
  constructor() {}

  ngOnInit(): void {}
  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    console.log("Button ok clicked!");
    this.isVisible = false;
  }

  handleCancel(): void {
    console.log("Button cancel clicked!");
    this.isVisible = false;
  }
}
