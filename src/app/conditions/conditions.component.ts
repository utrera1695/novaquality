import { Component, OnInit, Input, OnChanges } from "@angular/core";
import { DragulaService } from "ng2-dragula";
import { Random } from "mockjs";
import { CdkDragDrop, moveItemInArray } from "@angular/cdk/drag-drop";
@Component({
  selector: "app-conditions",
  templateUrl: "./conditions.component.html",
  styleUrls: ["./conditions.component.scss"]
})
export class ConditionsComponent implements OnChanges {
  public conditions: any = [];
  @Input() segments: number;
  constructor() {
    console.log(this.segments);
    for (let i = 0; i < this.segments; i++) {
      let condition = {
        title: Random.word()
      };
      this.conditions.push(condition);
    }
    /* this.condition = this.condition.concat(condition); */
  }

  ngOnChanges(changes): void {
    console.log(this.segments, changes);
    this.conditions = [];
    for (let i = 0; i < changes.segments.currentValue; i++) {
      let condition = {
        title: Random.word()
      };
      this.conditions.push(condition);
    }
    console.log(this.conditions);
  }
  drop(event: CdkDragDrop<string[]>) {
    console.log(document.getElementsByClassName("cdk-drag-dragging"));
    moveItemInArray(this.conditions, event.previousIndex, event.currentIndex);
  }
}
