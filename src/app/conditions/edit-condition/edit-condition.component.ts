import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-edit-condition",
  templateUrl: "./edit-condition.component.html",
  styleUrls: ["./edit-condition.component.scss"]
})
export class EditConditionComponent implements OnInit {
  @Input() title: String;
  isVisible = false;
  constructor() {}

  ngOnInit(): void {}
  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    console.log("Button ok clicked!");
    this.isVisible = false;
  }

  handleCancel(): void {
    console.log("Button cancel clicked!");
    this.isVisible = false;
  }
}
