import { Component, OnInit } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-new-condition",
  templateUrl: "./new-condition.component.html",
  styleUrls: ["./new-condition.component.scss"]
})
export class NewConditionComponent implements OnInit {
  isVisible = false;
  constructor() {}

  ngOnInit(): void {}
  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    console.log("Button ok clicked!");
    this.isVisible = false;
  }

  handleCancel(): void {
    console.log("Button cancel clicked!");
    this.isVisible = false;
  }
}
