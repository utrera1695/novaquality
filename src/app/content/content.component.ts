import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-content",
  templateUrl: "./content.component.html",
  styleUrls: ["./content.component.scss"]
})
export class ContentComponent implements OnInit {
  segmentModel = 1;
  segments = 0;
  constructor() {}

  ngOnInit(): void {}
  change(evnt) {
    console.log(evnt);
    this.segments = evnt;
  }
}
