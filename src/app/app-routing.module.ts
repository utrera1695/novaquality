import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ContentComponent } from "./content/content.component";
import { LoginComponent } from "./login/login.component";
import { SegmentationComponent } from './segmentation/segmentation.component';
import { ConditionsComponent } from './conditions/conditions.component';
import { ClausesComponent } from './clauses/clauses.component';
import { NewclausesComponent } from './clauses/newclauses/newclauses.component';
import { NewsegmentationComponent } from './segmentation/newsegmentation/newsegmentation.component';
import { TreemapComponent } from './clauses/treemap/treemap.component';

const routes: Routes = [
  { path: "", component: ContentComponent },
  { path: "segmentation", component: SegmentationComponent },
  { path: "segmentation/new", component: NewsegmentationComponent },
  { path: "treemap", component: TreemapComponent },
  { path: "conditions", component: ConditionsComponent },
  { path: "clauses", component: ClausesComponent },
  { path: "clauses/new", component: NewclausesComponent },
{ path: "**", redirectTo: "" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
